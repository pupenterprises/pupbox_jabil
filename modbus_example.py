import minimalmodbus
from time import sleep

instrument = minimalmodbus.Instrument('/dev/ttyUSB0',1)

#instrument.debug = True
#instrument.handle_local_echo = True # The serial device echos back every write, hence this
instrument.serial.baudrate = 9600
instrument.serial.timeout = 1

#value = instrument.read_register(0,2)

#print(value)
for i in range(30):	
	value = instrument.read_register(i,1)
	print(f"Leitura {i}-------------------------")
	print(value)
	#sleep(.2)

value = instrument.read_register(0,1)	
print(f"Leitura V L1")
print(value)

value = instrument.read_register(2,1)	
print(f"Leitura V L2")
print(value)

value = instrument.read_register(4,1)	
print(f"Leitura V L3")
print(value)

value = instrument.read_register(12,1)	
print(f"Leitura A L1")
print(f"valor bruto - " + str(value))
print(f"valor calculado - " + str(value/30))

value = instrument.read_register(14,1)	
print(f"Leitura A L2")
print(f"valor bruto - " + str(value))
print(f"valor calculado - " + str(value/30))

value = instrument.read_register(16,1)	
print(f"Leitura A L3")
print(f"valor bruto - " + str(value))
print(f"valor calculado - " + str(value/30))

#sleep(.2)
