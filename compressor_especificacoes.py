

class Especificacoes():
	def __init__(self):
		self.perifericos_testar = False
		self.perifericos_voltagem_v1_min = 0
		self.perifericos_voltagem_v1_max = 0
		self.perifericos_voltagem_v2_min = 0
		self.perifericos_voltagem_v2_max = 0
		self.perifericos_voltagem_v3_min = 0
		self.perifericos_voltagem_v3_max = 0
		self.partida_compressor_testar = 0
		self.partida_compressor_tempo = 0					
		self.corrente_compressor_testar = False
		self.corrente_compressor_repetir = 0
		self.corrente_compressor_l1_min = 0
		self.corrente_compressor_l1_max = 0
		self.corrente_compressor_l2_min = 0
		self.corrente_compressor_l2_max = 0
		self.corrente_compressor_l3_min = 0
		self.corrente_compressor_l3_max = 0
		self.inpecao_visual_testar = False
		self.inpecao_visual_tempo = inpecao_visual_tempo
		self.compressor_code = compressor_code
		
	def criar_novo(self,modelo, tipo_alimentacao, voltagem):
		self.perifericos_testar = True
		self.partida_compressor_testar = True
		self.partida_compressor_tempo = 5	
		self.corrente_compressor_testar = True
		self.corrente_compressor_repetir = 30
		
		if voltagem=="220V" and tipo_alimentacao == "MONOFASICO":
			self.perifericos_voltagem_v1_min = 198
			self.perifericos_voltagem_v1_max = 242
			self.perifericos_voltagem_v2_max = 0
			self.perifericos_voltagem_v3_min = 0
			self.perifericos_voltagem_v3_max = 0
			self.corrente_compressor_l1_min = 7.5
			self.corrente_compressor_l1_max = 14
			self.corrente_compressor_l2_min = 0
			self.corrente_compressor_l2_max = 0
			self.corrente_compressor_l3_min = 0
			self.corrente_compressor_l3_max = 0
			
		if voltagem=="220V" and tipo_alimentacao == "TRIFASICO":
			self.perifericos_voltagem_v1_min = 198
			self.perifericos_voltagem_v1_max = 242
			self.perifericos_voltagem_v2_min = 198
			self.perifericos_voltagem_v2_max = 242
			self.perifericos_voltagem_v3_min = 198
			self.perifericos_voltagem_v3_max = 242
			self.corrente_compressor_l1_min = 7.5
			self.corrente_compressor_l1_max = 14
			self.corrente_compressor_l2_min = 0
			self.corrente_compressor_l2_max = 0
			self.corrente_compressor_l3_min = 0
			self.corrente_compressor_l3_max = 0
			
			
			
		
