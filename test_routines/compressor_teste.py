import minimalmodbus
import time
#from models.models import *
from models.models import *
#from models import *

comandos = { 
			 "380V_TRIFASICO": 1280,
			 "220V_TRIFASICO": 1281,
			 "220V_MONOFASICO": 1282,
			 "EXTRA": 1283,				
			}
			
leitura = {
			"L1_V":0,
			"l2_V":2,
			"L3_V":4,
			"L1_A":12,
			"L2_A":14,
			"L3_A":16
			}



resultado_teste ={
		"mensagem":"",
		"valor_medido":0.0,
		"resultado":true
}

class Compressor_Teste():
	def __init__(self):
		self.model = Models()
		sensor = minimalmodbus.Instrument('/dev/ttyUSB0', 1) 
		sensor.serial.baudrate=9600 
		time.sleep(1)
		clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
		clp.serial.baudrate=19200
		self.codigo_compressor=""
		self.passos_teste = {
		"ler_codigo_barras":true,
		"verificar_periféricos":true,
		"partida_compressor":true,
		"leitura_corrente_compressor":true,
		"leitura_corrente_ventilador":true,
		"leitura_pressao":true,
		"teste_rotação":true
		}
		self.resultado_teste ={
		"mensagem":"",
		"valor_medido":0.0,
		"resultado":true
		}
		self.plano_teste=[]
		print("Compressor teste")
		
	def testar(self,passo):	
		print(passo)
		if plano_teste[passo]==True:	
			if passo=="ler_codigo_barras":
				resultado=self.ler_codigo_barras(codigo)
			elif passo== "verificar_periféricos":
				resultado=self.verificar_periféricos()
			elif passo=="partida_compressor":
				resultado=self.partida_compressor()
			elif passo=="leitura_corrente_compressor":
				resultado=self.leitura_corrente_compressor()
			elif passo=="leitura_corrente_ventilador":	
				resultado=self.leitura_corrente_ventilador()
			elif passo=="leitura_pressao":	
				resultado=self.leitura_pressao()
			elif passo=="teste_rotação":
				resultado=self.teste_rotação()
		else:
			resultado=passo + " para de me enganar desativado"
		return resultado
		
	def atualizar_modelo(self,codigo):
		self.modelo = self.model.read_model(codigo)
		self.atualizar_plano_teste()
		print(self.modelo.nome)
		print(self.modelo.codigo)
		print(self.modelo.btus)
		print(self.modelo.modo_temperatura)
		print(self.modelo.tipo_alimentacao)
		print(self.modelo.voltagem)
		
	def atualizar_plano_teste(self):
		self.plano_teste["ler_codigo_barras"]=True
		self.plano_teste["verificar_periféricos"]=True
		self.plano_teste["partida_compressor"]=True
		self.plano_teste["leitura_corrente_compressor"]=True
		self.plano_teste["leitura_corrente_ventilador"]=True
		self.plano_teste["leitura_pressao"]=True
		self.plano_teste["teste_rotação"]=True
		
			
	def ler_codigo_barras(self,codigo):
		atualizar_modelo(codigo)
		print("ler_codigo_barras")
		
	def verificar_periféricos(self):
		#energizar o sistema
		clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
		clp.serial.baudrate=19200
		clp.write_bit(comandos["380V_TRIFASICO"],1)
		time.sleep(4)
		clp.write_bit(comandos["380V_TRIFASICO"],0)
		clp.write_bit(comandos["220V_TRIFASICO"],1)
		time.sleep(4)
		clp.write_bit(comandos["220V_TRIFASICO"],0)
		clp.write_bit(comandos["220V_MONOFASICO"],1)
		time.sleep(4)
		clp.write_bit(comandos["220V_MONOFASICO"],0)
		clp.write_bit(comandos["EXTRA"],1)
		time.sleep(4)
		clp.write_bit(comandos["EXTRA"],0)
		print("verificar_periféricos")	
		
	def partida_compressor(self):
		try:
			clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			clp.serial.baudrate=19200
			clp.write_bit(comandos["220V_MONOFASICO"],0)
			clp.write_bit(comandos["220V_TRIFASICO"],0)
			clp.write_bit(comandos["380V_TRIFASICO"],0)
			if self.modelo.tipo_alimentacao == "220V_MONOFASICO":
				clp.write_bit(comandos["220V_MONOFASICO"],1)
			elif self.modelo.tipo_alimentacao == "220V_TRIFASICO":
				clp.write_bit(comandos["220V_TRIFASICO"],1)
			elif self.modelo.tipo_alimentacao == "380V_TRIFASICO":
				clp.write_bit(comandos["380V_TRIFASICO"],1)	
				
			self.resultado_teste['mensagem']="Partida compressor "+self.modelo.tipo_alimentacao
			self.resultado_teste['valor_medido']=0.0
			self.resultado_teste['resultado']=True
		except:	
			self.resultado_teste['mensagem']="Partida compressor "+self.modelo.tipo_alimentacao
			self.resultado_teste['valor_medido']=0.0
			self.resultado_teste['resultado']=False
			
		print("partida_compressor")
		return self.resultado_teste
		
	def leitura_corrente_compressor(self):
		
	def leitura_corrente_ventilador(self):
		print("leitura_corrente_ventilador")
		
	def leitura_pressao(self):
		print("leitura_pressao")
		
	def teste_rotação(self):
		print("teste_rotação")
		
	def verificar_tensões():
		test =  instrument.read_register(1,1)
		for item in test:
			print(item)
		
if __name__ == "__main__":
	teste = Compressor_Teste()
	#teste.executar("1234")
	teste.modelo.tipo_alimentacao = "220V_MONOFASICO"
	teste.partida_compressor()
	teste.modelo.tipo_alimentacao = "220V_TRIFASICO"
	teste.partida_compressor()
	teste.modelo.tipo_alimentacao = "380V_TRIFASICO"
	teste.partida_compressor()

