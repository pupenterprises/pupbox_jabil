"""
Demo Flask application to test the operation of Flask with socket.io

Aim is to create a webpage that is constantly updated with random numbers from a background python process.

30th May 2014

===================

Updated 13th April 2018

+ Upgraded code to Python 3
+ Used Python3 SocketIO implementation
+ Updated CDN Javascript and CSS sources

"""
# Start with a basic flask app webpage.
from flask_socketio import SocketIO, emit,send
from flask_assets import Bundle, Environment
from flask import Flask, render_template, url_for, copy_current_request_context,flash,request,redirect,url_for
from random import random
from time import sleep
from threading import Thread, Event
from models.models import *
from compressor_teste import Compressor_Teste, passos_teste
from flask_session import Session
from test_log import Test_log
import json


__author__ = 'powerup innovation'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

socketio = SocketIO(app, async_mode=None, logger=True, engineio_logger=False)
model = Models()


compressor_teste = Compressor_Teste()

'''
thread = Thread()
thread_stop_event = Event()
'''
#def jigCompressores():

@app.route('/configuracao',methods=["GET","POST"])
def configuracao():
	if request.method == "POST":
		ftp = request.form.get("ftp")
		endereco = request.form.get("endereco")
		ip = request.form.get("ip")
		porta = request.form.get("porta")
		maquina = request.form.get("maquina")
		fvt = request.form.get("fvt")
		configuracao = model.get_configurations()
		print(configuracao)
		if (configuracao == []):
			print("criando configuracoes")
			model.create_configuration( ftp=ftp,
										address=endereco,
										port=porta,
										ip=ip,
										maquina=maquina,
										fvt=fvt
										)
		else:
			print("atualizando configuracoes")	
			model.update_configuration(	ftp=ftp,
										address=endereco,
										port=porta,
										ip=ip,
										maquina=maquina,
										fvt=fvt)	
	query = model.get_configurations()
	if query:
		configuracoes = query
	else:
		configuracoes = None
	print(configuracoes)
	#print(configuracoes.ftp+" "+configuracoes.address+" "+configuracoes.port+" "+configuracoes.ip)
	return render_template('configuracao.html',configuracoes=configuracoes)


@app.route('/cadastro_especificacoes/<modelo>',methods=["GET","POST"])
def cadastro_especificacoes(modelo=None):	
	if request.method == "POST":
		#print("Salvando especificacoes")
		if True:
			print("Salvando especificacoes")
			perifericos_voltagem_v1_min = float(request.form.get("voltagem_V1_minima"))
			perifericos_voltagem_v1_max = float(request.form.get("voltagem_V1_maxima"))
			perifericos_voltagem_v2_min = float(request.form.get("voltagem_V2_minima"))
			perifericos_voltagem_v2_max = float(request.form.get("voltagem_V2_maxima"))
			perifericos_voltagem_v3_min = float(request.form.get("voltagem_V3_minima"))
			perifericos_voltagem_v3_max = float(request.form.get("voltagem_V3_maxima"))
			partida_compressor_tempo = request.form.get("partida_tempo")
			corrente_compressor_repetir = request.form.get("corrente_compressor_tempo")						
			corrente_compressor_l1_min = float(request.form.get("compressor_l1_minima"))
			corrente_compressor_l1_max = float(request.form.get("compressor_l1_maxima"))
			corrente_compressor_l2_min = float(request.form.get("compressor_l2_minima"))
			corrente_compressor_l2_max = float(request.form.get("compressor_l2_maxima"))
			corrente_compressor_l3_min = float(request.form.get("compressor_l3_minima"))
			corrente_compressor_l3_max = float(request.form.get("compressor_l3_maxima"))
			inpecao_visual_tempo = request.form.get("inspecao_tempo")				
			compressor_code = request.form.get("codigo")
			especificacoes_compressor = model.read_specification(compressor_code)
			if especificacoes_compressor:
				model.update_specification(	perifericos_testar = True,
											perifericos_voltagem_v1_min = perifericos_voltagem_v1_min,
											perifericos_voltagem_v1_max = perifericos_voltagem_v1_max,
											perifericos_voltagem_v2_min = perifericos_voltagem_v2_min,
											perifericos_voltagem_v2_max = perifericos_voltagem_v2_max,
											perifericos_voltagem_v3_min = perifericos_voltagem_v3_min,
											perifericos_voltagem_v3_max = perifericos_voltagem_v3_max,
											partida_compressor_testar = True,
											partida_compressor_tempo = partida_compressor_tempo,
											corrente_compressor_testar = True,
											corrente_compressor_repetir = corrente_compressor_repetir,						
											corrente_compressor_l1_min = corrente_compressor_l1_min,
											corrente_compressor_l1_max = corrente_compressor_l1_max,
											corrente_compressor_l2_min = corrente_compressor_l2_min,
											corrente_compressor_l2_max = corrente_compressor_l2_max,
											corrente_compressor_l3_min = corrente_compressor_l3_min,
											corrente_compressor_l3_max = corrente_compressor_l3_max,	
											inpecao_visual_testar = True,
											inpecao_visual_tempo = inpecao_visual_tempo,					
											compressor_code = compressor_code
										)
			else:
				model.create_specification(	perifericos_testar = True,
											perifericos_voltagem_v1_min = perifericos_voltagem_v1_min,
											perifericos_voltagem_v1_max = perifericos_voltagem_v1_max,
											perifericos_voltagem_v2_min = perifericos_voltagem_v2_min,
											perifericos_voltagem_v2_max = perifericos_voltagem_v2_max,
											perifericos_voltagem_v3_min = perifericos_voltagem_v3_min,
											perifericos_voltagem_v3_max = perifericos_voltagem_v3_max,
											partida_compressor_testar = True,
											partida_compressor_tempo = partida_compressor_tempo,
											corrente_compressor_testar = True,
											corrente_compressor_repetir = corrente_compressor_repetir,						
											corrente_compressor_l1_min = corrente_compressor_l1_min,
											corrente_compressor_l1_max = corrente_compressor_l1_max,
											corrente_compressor_l2_min = corrente_compressor_l2_min,
											corrente_compressor_l2_max = corrente_compressor_l2_max,
											corrente_compressor_l3_min = corrente_compressor_l3_min,
											corrente_compressor_l3_max = corrente_compressor_l3_max,	
											inpecao_visual_testar = True,
											inpecao_visual_tempo = inpecao_visual_tempo,					
											compressor_code = compressor_code
										)
			modelo = compressor_code
			#return especificacoes()
	print("especificacoes_compressor")
	
	especificacoes_compressor = model.read_specification(modelo)
	#print(especificacoes_compressor.compressor_code)	
	return render_template('cadastro_especificacoes.html',especificacoes_compressor=especificacoes_compressor)
	

@app.route('/especificacoes',methods=["GET","POST"])
def especificacoes():	
	if request.method == 'POST':
		codigo = request.form.get("codigo")
		if request.form.get("editar"):	
			print(codigo)
			#especificacoes_compressor = model.read_specification(codigo)	
			
			return redirect(url_for('cadastro_especificacoes',modelo=codigo))

	modelos_compressores = model.get_all_models()
	return render_template('especificacoes.html',especificacoes=modelos_compressores)

	
	
	
@app.route('/cadastro_modelos/<modelo>')	
@app.route('/cadastro_modelos',methods=["GET","POST"])
def cadastro_modelos(modelo=None):
	if request.method == "POST":
		codigo = request.form.get("codigo")
		cliente = request.form.get("cliente")
		nome = request.form.get("nome")
		btus = request.form.get("btus")
		modo_temperatura = request.form.get("modo_temperatura")
		tipo_alimentacao = request.form.get("tipo_alimentacao")
		voltagem = request.form.get("voltagem")
		print(codigo+","+nome+","+btus+","+modo_temperatura+","+tipo_alimentacao+","+voltagem)
		if codigo and nome and modo_temperatura and tipo_alimentacao and voltagem:
			modelo=model.read_model(codigo)
			if modelo:
				print("editando")
				model.update_model(nome=nome,cliente=cliente,codigo=codigo,btus=btus,modo_temperatura=modo_temperatura,tipo_alimentacao=tipo_alimentacao,voltagem=voltagem)				
			else:
				print("criando novo")	
				model.create_model(nome=nome,cliente=cliente, codigo=codigo,btus=btus,modo_temperatura=modo_temperatura,tipo_alimentacao=tipo_alimentacao,voltagem=voltagem)
			return modelos()			
	return render_template('cadastro_modelos.html')


@app.route('/modelos',methods=["GET","POST"])
def modelos():
	if request.method == 'POST':
		codigo = request.form.get("codigo")
		if request.form.get("editar"):	
			modelo=model.read_model(codigo)		
			return render_template('cadastro_modelos.html',modelo=modelo)
		elif request.form.get("excluir"):
			print ("excluir")
			model.delete_model(codigo)       
	else:
		print ("test_2")  
	modelos_compressores = model.get_all_models()
	#print(modelos)
	return render_template('modelos.html',modelos_compressores=modelos_compressores)
	
@app.route('/',methods=["GET","POST"])
def index():
	barcode= None
	modelo = None
	testes = []
	if request.method == 'POST':
		print("Executar teste")
		barcode = request.form.get("barcode")				
		if len(barcode)>6:		
			compressor_teste.ler_codigo_barras(barcode)
			print(barcode)
			print(barcode[0:8])
			modelo = model.read_model(barcode[0:8])
			
	logs = model.read_test_log_all()
	indice =0
	if logs:
		for teste in logs:
			indice = indice + 1
			testes.append(teste.resultado)
	for i in range(15-indice):
		testes.append("Passed")
	print(testes)
	return render_template('index.html',modelo=modelo,barcode=barcode,testes=testes)
	
def messageReceived(methods=['GET', 'POST']):
    print('message was received!!!')
	
@socketio.on('teste',namespace='/')
def handle_teste(txt):	
	print ("testando socket"+str(txt))
	emit('teste_log',{'mensagem':"Iniciando","valor_medido":0.0,"resultado":True},callback=messageReceived)
	compressor_teste.andon("TESTANDO")
	configuracao = model.get_configurations()
	
	for passo in passos_teste:
		print("passo {passo}",passo)
		
		if passo == "leitura_corrente_compressor":
			repete_teste=30
		else:
			repete_teste=1
		for repeticoes in  range(repete_teste):
			resultado=compressor_teste.testar(passo)
			print (resultado)
			emit('teste_log',resultado,callback=messageReceived)
			if passo == "ler_codigo_barras":
				
				teste_log = Test_log(serial=resultado['serial'],
							cliente=resultado['cliente'],
							maquina=configuracao.maquina,
							fvt=configuracao.fvt)
			else:
				if resultado['mensagem']!= "desativado":		
					if resultado['resultado']==False:
						compressor_teste.andon("REPROVADO")
						emit('teste_log',{'mensagem':"Finalizando","valor_medido":0.0,"resultado":False},callback=messageReceived)
						teste_log.adicionar_log_teste(
												resultado="Fail",
												sintoma=resultado["sintoma"],
												descricao=resultado["label_medicao"],
												valor=resultado["valor_medicao"] ,
												unidade=resultado["unidade"])
						teste_log.salvar_em_banco()
						print("arquivo de log criado")
						return
					else:
						teste_log.adicionar_log_teste(
												resultado = "Passed",
												sintoma=resultado["sintoma"],
												descricao=resultado["label_medicao"],
												valor=resultado["valor_medicao"] ,
												unidade=resultado["unidade"])
						print("arquivo de log criado")
				
				
			sleep(1)
	else:
		compressor_teste.andon("APROVADO")
		teste_log.salvar_em_banco()
		emit('teste_log',{'mensagem':"Finalizando","valor_medido":0.0,"resultado":True},callback=messageReceived)
		



if __name__ == '__main__':
	socketio.run(app)
		
		
s
