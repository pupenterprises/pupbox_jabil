import minimalmodbus
import time
import json
#from models.models import *
from models.models import *
#from models import *



comandos = { 
			 "380V_TRIFASICO": 1280,
			 "220V_TRIFASICO": 1281,
			 "220V_MONOFASICO": 1282,
			 "VALVULA_REVERSORA": 1283,
			 "SINALIZACAO_380_TRIFASICA": 1285,	
			 "SINALIZACAO_220_TRIFASICA": 1286,	
			 "SINALIZACAO_220_MONOFASICA": 1287,
			 "ANDON_AMARELO": 1288,	
			 "ANDON_VERDE": 1289,	
			 "ANDON_VERMELHO": 1290,			 					
			}
			
botoeiras = {
			  "INICIAR":1024,
			  "APROVAR":1025,
			  "REPROVAR":1026
			}

			}
			
leitura = {
			"L1_V":0,
			"l2_V":4,
			"L3_V":2,
			"L12_V":6,
			"l23_V":8,
			"L32_V":10,
			"L1_A":12,
			"L2_A":14,
			"L3_A":16
			}
	
passos_teste = [
		"ler_codigo_barras",
		"verificar_periféricos",
		"partida_compressor",
		"leitura_voltagem_compressor",
		"leitura_corrente_compressor",
		"leitura_corrente_ventilador",
		"leitura_pressao",
		"teste_rotação",
		"inspecao_visual"
		]
		


class Compressor_Teste():
	def __init__(self):
		self.model = Models()
		try:
			self.sensor = minimalmodbus.Instrument('/dev/ttyUSB0', 1) 
			self.sensor.serial.baudrate=9600 
			self.sensor.serial.timeout=1
			time.sleep(1)
			self.clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			self.clp.serial.baudrate=19200
		except:
			print("falha na comunicaço com periféricos")
		self.configuracao = self.model.get_configurations()	
		self.codigo_compressor=""
		self.plano_teste={}
		self.atualizar_plano_teste()
		self.serial=None
		self.cliente = None	
		self.especificacoes=None
		
	
	def testar(self,passo):		 
		if self.plano_teste[passo]==True:	
			if passo=="ler_codigo_barras":
				resultado=self.ler_codigo_barras()
			elif passo== "verificar_periféricos":
				resultado=self.verificar_periféricos()
			elif passo=="partida_compressor":
				resultado=self.partida_compressor()
			elif passo=="leitura_corrente_compressor":
				resultado=self.leitura_corrente_compressor()
			elif passo=="leitura_voltagem_compressor":
				resultado=self.leitura_voltagem_compressor()
			elif passo=="leitura_corrente_ventilador":	
				resultado=self.leitura_corrente_ventilador()
			elif passo=="leitura_pressao":	
				resultado=self.leitura_pressao()
			elif passo=="teste_rotação":
				resultado=self.teste_rotação()
			elif passo=="inspecao_visual":
				resultado=self.inspecao_visual()
				
		else:
			resultado= self.montar_resultado(mensagem="Desativado")
			
		resultado['serial'] = self.serial
		resultado['cliente'] = self.modelo.cliente
		
		return resultado
	
	def montar_resultado(self,	mensagem=None, 
								sintoma=None,
								label_medicao=None,
								valor_medicao=None,
								medicao=None,
								unidade=None,
								resultado=None,
								especificacoes=None,
								inspecao=False,
								logavel=False):
		resultado_teste = {}
		#resultado_teste["maquina"] = self.configuracao.maquina
		#resultado_teste["fvt"] = self.configuracao.maquina
		resultado_teste["mensagem"] = mensagem
		resultado_teste["sintoma"] = sintoma					
		resultado_teste["label_medicao"] = label_medicao
		resultado_teste["valor_medicao"] = valor_medicao
		resultado_teste["medicao"] = medicao
		resultado_teste["unidade"] = unidade
		resultado_teste["resultado"] = resultado
		resultado_teste["especificacoes"] = especificacoes
		resultado_teste["logavel"] = logavel
		resultado_teste["inspecao_visual"] = inspecao
		return resultado_teste
		
		
	
	def atualizar_plano_teste(self):
		self.plano_teste["ler_codigo_barras"]=False
		self.plano_teste["verificar_periféricos"]=True#False
		self.plano_teste["partida_compressor"]=True
		self.plano_teste["leitura_corrente_compressor"]=True
		self.plano_teste["leitura_voltagem_compressor"]=True
		self.plano_teste["leitura_corrente_ventilador"]=False
		self.plano_teste["leitura_pressao"]=False
		self.plano_teste["teste_rotação"]=False	
		self.plano_teste["inspecao_visual"]=True	
	
	
	def ler_codigo_barras(self,codigo):
		self.modelo=None
		self.serial=codigo
		self.modelo = self.model.read_model(codigo[0:8])
		self.especificacoes = self.model.read_specification(codigo[0:8])
		print(f" especificacoes {self.especificacoes.perifericos_voltagem_v1_min}")
		self.sinalizadora()
		if self.modelo:
			self.atualizar_plano_teste()
			print(self.modelo.nome)
			print(self.modelo.codigo)
			print(self.modelo.btus)
			print(self.modelo.modo_temperatura)
			print(self.modelo.tipo_alimentacao)
			print(self.modelo.voltagem)	
			resultado = self.montar_resultado(	mensagem = "Codigo de barras lido",
												resultado = True,
												logavel = False												
											 )			

		else:
			resultado = self.montar_resultado(	mensagem = "Falha ao ler o Codigo de barras",
												resultado = True,
												logavel = False												
											 )	

		return resultado
		
	def verificar_periféricos(self):
		if True:
			resultado = self.montar_resultado(	mensagem = "Verficar perifericos",
												resultado = True,
												logavel = False												
											 )

		else:
			resultado = self.montar_resultado(	mensagem = "Falha nos perifericos",
												resultado = False,
												logavel = False												
											 )		
		return resultado
		
		
	def teste_valvula_reversora(self):
		try:
			self.clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			self.clp.serial.baudrate=19200
			self.clp.write_bit(comandos["VALVULA_REVERSORA"],1)			
			self.resultado_teste["mensagem"] = "Teste de valulvar reversora"
			self.resultado_teste["sintoma"] = "aprovado"
			self.resultado_teste["medicao"] = "ok"
			self.resultado_teste["unidade"] = "ok"
			self.resultado_teste["resultado"] = True			
		except:			
			self.resultado_teste["mensagem"] = "Falha ao teste de valvula"
			self.resultado_teste["valor_medido"] = 0.0
			self.resultado_teste["resultado"] = False
		return self.resultado_teste
			
	
		
	def partida_compressor(self):
		try:
			self.clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			self.clp.serial.baudrate=19200
			self.clp.write_bit(comandos["220V_MONOFASICO"],0)
			self.clp.write_bit(comandos["220V_TRIFASICO"],0)
			self.clp.write_bit(comandos["380V_TRIFASICO"],0)
			if self.modelo.tipo_alimentacao == "MONOFASICO" and self.modelo.voltagem == "220V": 
				self.clp.write_bit(comandos["220V_MONOFASICO"],1)
			elif self.modelo.tipo_alimentacao == "TRIFASICO" and self.modelo.voltagem == "220V":
				self.clp.write_bit(comandos["220V_TRIFASICO"],1)
			elif self.modelo.tipo_alimentacao == "TRIFASICO" and self.modelo.voltagem == "380V":
				self.clp.write_bit(comandos["380V_TRIFASICO"],1)		
				
			print("partida_compressor")
			resultado = self.montar_resultado( mensagem = "Partida",
												logavel = True,
												resultado=True,
												inspecao = True)
		except:
			resultado = self.montar_resultado( mensagem = "Falha na partida",
											   logavel = True,
											   resultado=True,
											   inspecao = True)
			
		return resultado
		
	def leitura_voltagem_compressor(self,tempo_teste=1):
		tensao_l12 = 0
		tensao_l23 = 0
		tensao_l13 = 0
		try:
			self.sensor = minimalmodbus.Instrument('/dev/ttyUSB0', 1) 
			self.sensor.serial.baudrate=9600 
			self.sensor.serial.timeout=1
			
			for i in range(tempo_teste):
				tensao_l12 = self.sensor.read_register(6,1)
				tensao_l23 = self.sensor.read_register(8,1)
				tensao_l13 = self.sensor.read_register(10,1)
				
				print(self.modelo.tipo_alimentacao)
				print(self.modelo.voltagem)
				print(f"voltagens {tensao_l12} {tensao_l23} {tensao_l13}")
				print("<{0:.2f}><{1:.2f}> <{2:.2f}><{3:.2f}> <{4:.2f}><{5:.2f}>".format(self.especificacoes.perifericos_voltagem_v1_min,self.especificacoes.perifericos_voltagem_v1_max,self.especificacoes.perifericos_voltagem_v2_min,self.especificacoes.perifericos_voltagem_v2_max,self.especificacoes.perifericos_voltagem_v3_min,self.especificacoes.perifericos_voltagem_v3_max),
														)
				
				if 	self.especificacoes.perifericos_voltagem_v1_min <= tensao_l12 <= self.especificacoes.perifericos_voltagem_v1_max and \
					self.especificacoes.perifericos_voltagem_v2_min <= tensao_l23 <= self.especificacoes.perifericos_voltagem_v2_max and \
					self.especificacoes.perifericos_voltagem_v3_min <= tensao_l13 <= self.especificacoes.perifericos_voltagem_v3_max:
					resultado = self.montar_resultado(	mensagem = "Voltagem ok",
														sintoma = "aprovado",
														label_medicao = "Compressor",
														valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(tensao_l12,tensao_l23,tensao_l13),
														unidade = "V",
														especificacoes = "<{0:.2f}><{1:.2f}>".format(self.especificacoes.perifericos_voltagem_v1_min,self.especificacoes.perifericos_voltagem_v1_max),
														resultado= True,
														logavel = True	
														)	
				else:
					resultado = self.montar_resultado(	mensagem = "Voltagem ng",
														sintoma = "Out of spec",
														label_medicao = "Voltagem",
														valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(tensao_l12,tensao_l23,tensao_l13),
														unidade = "V",
														especificacoes = "<{0:.2f}><{1:.2f}>".format(self.especificacoes.perifericos_voltagem_v1_min,self.especificacoes.perifericos_voltagem_v1_max),
														resultado= False,
														logavel = True	
														)
												

					self.resetar_reles()	
					return resultado
		except:
				resultado = self.montar_resultado(	mensagem = "Voltagem",
													sintoma = "Falha equipamento",
													label_medicao = "Voltagem",
													valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(tensao_l12,tensao_l23,tensao_l13),
													unidade = "V",
													especificacoes = "<{0:.2f}><{1:.2f}>".format(self.especificacoes.perifericos_voltagem_v1_min,self.especificacoes.perifericos_voltagem_v1_max),
													resultado= True,
													logavel = True	
												)
				self.resetar_reles()
		return resultado
		
	def leitura_corrente_compressor(self,tempo_teste=1):
		corrente_l1 = 0
		corrente_l2 = 0
		corrente_l3 = 0
		try:
			resultado_voltagem = True
			resultado_corrente = True
			self.sensor = minimalmodbus.Instrument('/dev/ttyUSB0', 1) 
			self.sensor.serial.baudrate=9600 
			self.sensor.serial.timeout=1
			
			for i in range(tempo_teste):
				corrente_l1 = self.sensor.read_register(12,1)/25
				corrente_l2 = self.sensor.read_register(14,1)/25
				corrente_l3 = self.sensor.read_register(16,1)/25
				
				print(self.modelo.tipo_alimentacao)
				print(self.modelo.voltagem)
				print(f"correntes {corrente_l1} {corrente_l2} {corrente_l3}")
				
				if self.especificacoes.corrente_compressor_l1_min<=corrente_l1<=self.especificacoes.corrente_compressor_l1_max and \
				   self.especificacoes.corrente_compressor_l2_min<=corrente_l2<=self.especificacoes.corrente_compressor_l1_max and \
				   self.especificacoes.corrente_compressor_l3_min<=corrente_l3<=self.especificacoes.corrente_compressor_l1_max:
					resultado = self.montar_resultado(	mensagem = "Corrente do compressor",
														sintoma = "aprovado",
														label_medicao = "Compressor",
														valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(corrente_l1,corrente_l2,corrente_l3),
														unidade = "V",
														resultado= True,
														especificacoes = "<{0:.2f}><{1:.2f}>".format(self.especificacoes.corrente_compressor_l1_min,self.especificacoes.corrente_compressor_l1_max),
													
														logavel = True	
														)
				else:
					resultado = self.montar_resultado(	mensagem = "Falha na voltage do compressor",
														sintoma = "Fora da especificacao",
														label_medicao = "Voltagem do compressor",
														valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(corrente_l1,corrente_l2,corrente_l3),
														unidade = "A",
														resultado= False,
														especificacoes = "{0:.2f} {1:.2f}".format(self.especificacoes.corrente_compressor_l1_min,self.especificacoes.corrente_compressor_l1_max),
														logavel = True	
														)
													

					self.resetar_reles()	
					return resultado
						   
				   
				
				'''
				if self.modelo.tipo_alimentacao == "MONOFASICO" and self.modelo.voltagem=="220V":
					print("MONOFASICO_220V")
					if 0<=tensao_l12<=242 and 0<=tensao_l23<=5 and 0<=tensao_l13<=5:
						print("ok")
					else:
						resultado_voltagem=True
						print("falha tensão")
						
					if 5<=corrente_l1<=17 and 5<=corrente_l2<=17 and 0<=corrente_l3<=2:
						print("ok")
					else:
						resultado_corrente=False
						print("falha corrente") 
											
					
				elif self.modelo.tipo_alimentacao == "TRIFASICO" and self.modelo.voltagem=="220V":
					print("TRIFASICO_220V")
					if 198<=tensao_l12<=242 and 198<=tensao_l23<=242 and 0<=tensao_l13<=5:
						print("ok")
					else:
						resultado_voltagem=False
						print("falha")
					if 5<=corrente_l1<=10 and 5<=corrente_l2<=10 and 5<=corrente_l3<=10:
						print("ok")
					else:
						resultado_corrente=False
						print("falha") 
					
				elif self.modelo.tipo_alimentacao == "TRIFASICO" and self.modelo.voltagem=="380V":
					print("TRIFASICO_380V")
					if 198<=tensao_l12<=242 and 198<=tensao_l23<=242 and 198<=tensao_l13<=242:
						
						print("ok")
					else:
						resultado_voltagem=False
						print("falha")
					
					if 198<=corrente_l1<=242 and 198<=corrente_l2<=242 and 198<=corrente_l3<=242:
						print("ok")
					else:
						resultado_corrente=False
						print("falha") 
				
				time.sleep(1)
				print("leitura_corrente_compressor")
				
				if resultado_corrente == True:
					resultado = self.montar_resultado(	mensagem = "Corrente do compressor",
														sintoma = "aprovado",
														label_medicao = "Compressor",
														valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(corrente_l1,corrente_l2,corrente_l3),
														unidade = "V",
														resultado= True,
														logavel = True	
														)

			
				else:
					resultado = self.montar_resultado(	mensagem = "Falha na voltage do compressor",
														sintoma = "Fora da especificacao",
														label_medicao = "Voltagem do compressor",
														valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(corrente_l1,corrente_l2,corrente_l3),
														unidade = "A",
														resultado= False,
														logavel = True	
														)
													

					self.resetar_reles()	
					return resultado
				''' 
		except:
				resultado = self.montar_resultado(	mensagem = "Corrente",
													sintoma = "Falha no sistema",
													label_medicao = "Corrente compressor",
													valor_medicao = "{0:.2f} {1:.2f} {2:.2f}".format(corrente_l1,corrente_l2,corrente_l3),
													unidade = "A",
													resultado= True,
													inspecao=True,
													especificacoes = "{0:.2f} {1:.2f}".format(self.especificacoes.corrente_compressor_l1_min,self.especificacoes.corrente_compressor_l1_max),
													logavel = True	
												)
		#self.resetar_reles()
		return resultado
				
		
	def leitura_corrente_ventilador(self):
		print("leitura_corrente_ventilador")
		self.resultado_teste["mensagem"] = "leitura_corrente_ventilador"
		self.resultado_teste["valor_medido"] = 0.0
		self.resultado_teste["resultado"] = True
		return self.resultado_teste
		
		
	def inspecao_visual(self):
		try:
			self.clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			self.clp.serial.baudrate=19200
			self.clp.write_bit(comandos["VALVULA_REVERSORA"],1)	
			resultado = self.montar_resultado(	mensagem = "Inspeção",
													sintoma = "Inspeção ok",
													label_medicao = "Inspeção",
													valor_medicao = "Não aplicável",
													unidade = "A",
													resultado= True,
													inspecao = True,
													especificacoes = "Não aplicável",
													logavel = True	
												)			
		except:				
			resultado = self.montar_resultado(	mensagem = "Inspeção",
													sintoma = "Inspeção ok",
													label_medicao = "Inspeção",
													valor_medicao = "Não aplicável",
													unidade = "A",
													resultado= True,
													inspecao=True,
													especificacoes = "Não aplicável",
													logavel = True	
												)		

		self.resetar_reles()
		return resultado
		
		
	def leitura_pressao(self):
		print("leitura_pressao")
		self.resultado_teste["mensagem"] = "leitura_pressao"
		self.resultado_teste["valor_medido"] = 0.0
		self.resultado_teste["resultado"] = True
		return self.resultado_teste
		
	def teste_rotação(self):
		print("teste_rotação")
		self.resultado_teste["mensagem"] = "teste_rotação"
		self.resultado_teste["valor_medido"] = 0.0
		self.resultado_teste["resultado"] = True
		return self.resultado_teste
		
	def verificar_tensões():
		test =  instrument.read_register(1,1)
		for item in test:
			print(item)
			
	def resetar_reles(self):
		try:
			self.clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			self.clp.serial.baudrate=19200
			self.clp.write_bit(comandos["220V_MONOFASICO"],0)
			self.clp.write_bit(comandos["220V_TRIFASICO"],0)
			self.clp.write_bit(comandos["380V_TRIFASICO"],0)
			self.clp.write_bit(comandos["VALVULA_REVERSORA"],0)
			self.clp.write_bit(comandos["SINALIZACAO_220_MONOFASICA"],0)
			self.clp.write_bit(comandos["SINALIZACAO_220_TRIFASICA"],0)
			self.clp.write_bit(comandos["SINALIZACAO_380_TRIFASICA"],0)
		except:
			print("falha ao resetar")
		
	
	def sinalizadora(self):
		try:
			self.clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			self.clp.serial.baudrate=19200
			self.clp.write_bit(comandos["SINALIZACAO_220_MONOFASICA"],0)
			self.clp.write_bit(comandos["SINALIZACAO_220_TRIFASICA"],0)
			self.clp.write_bit(comandos["SINALIZACAO_380_TRIFASICA"],0)
			print("sinalizadora {} {}",self.modelo.tipo_alimentacao,self.modelo.voltagem)
			if self.modelo.tipo_alimentacao == "MONOFASICO" and self.modelo.voltagem == "220V":
				self.clp.write_bit(comandos["SINALIZACAO_220_MONOFASICA"],1)
			elif self.modelo.tipo_alimentacao == "TRIFASICO" and self.modelo.voltagem == "220V":
				self.clp.write_bit(comandos["SINALIZACAO_220_TRIFASICA"],1)
			elif self.modelo.tipo_alimentacao == "TRIFASICO" and self.modelo.voltagem == "380V":
				self.clp.write_bit(comandos["SINALIZACAO_380_TRIFASICA"],1)
		except:
			print("falha na sializadora")
			
	def andon(self,test_status):
		try:
			self.clp = minimalmodbus.Instrument('/dev/ttyUSB0', 2) 
			self.clp.serial.baudrate=19200
			self.clp.write_bit(comandos["ANDON_VERDE"],0)
			self.clp.write_bit(comandos["ANDON_AMARELO"],0)
			self.clp.write_bit(comandos["ANDON_VERMELHO"],0)
			if test_status == "TESTANDO":
				self.clp.write_bit(comandos["ANDON_AMARELO"],1)
			elif test_status == "APROVADO":
				self.clp.write_bit(comandos["ANDON_VERDE"],1)
			elif test_status == "REPROVADO":
				self.clp.write_bit(comandos["ANDON_VERMELHO"],1)
		except:
			print("falha na sializadora")
		
if __name__ == "__main__":
	teste = Compressor_Teste()
	teste.ler_codigo_barras("RAP36F3L106003M")
	for passo in passos_teste:
		print("***************************")
		print("passo "+ passo)
		teste_resultado = teste.testar(passo)
		print("mensagem: 	" + str(teste_resultado["mensagem"]))
		print("sintoma: 	"+str(teste_resultado["sintoma"]))					
		print("l_medicao: 	"+ str(teste_resultado["label_medicao"]))
		print("v_medicao: 	"+ str(teste_resultado["valor_medicao"]))
		print("medicao: 	"+ str(teste_resultado["medicao"]))
		print("unidade: 	"+str(teste_resultado["unidade"]))
		print("resultado: 	"+ str(teste_resultado["resultado"]))

	
	
	'''
	teste.ler_codigo_barras("RAP36")
	#teste.executar("1234")
	print("220V_MONOFASICO")
	teste.modelo.tipo_alimentacao = "220V_MONOFASICO"
	teste.partida_compressor()
	teste.sinalizadora()
	time.sleep(4)
	
	print("220V_TRIFASICO")
	teste.modelo.tipo_alimentacao = "220V_TRIFASICO"
	teste.partida_compressor()
	teste.sinalizadora()
	time.sleep(4)
	print("380V_TRIFASICO")
	teste.modelo.tipo_alimentacao = "380V_TRIFASICO"
	teste.partida_compressor()
	teste.sinalizadora()
	print("380V_TRIFASICO")
	teste.modelo.tipo_alimentacao = "VALVULA_REVERSORA"
	teste.teste_valvula_reversora()
	time.sleep(4)
	teste.resetar_reles()
	'''
	
