"""
Demo Flask application to test the operation of Flask with socket.io

Aim is to create a webpage that is constantly updated with random numbers from a background python process.

30th May 2014

===================

Updated 13th April 2018

+ Upgraded code to Python 3
+ Used Python3 SocketIO implementation
+ Updated CDN Javascript and CSS sources

"""
# Start with a basic flask app webpage.
from flask_socketio import SocketIO, emit,send
from flask_assets import Bundle, Environment
from flask import Flask, render_template, url_for, copy_current_request_context,flash,request
from random import random
from time import sleep
from threading import Thread, Event
from models.models import *
from compressor_teste import Compressor_Teste, passos_teste
from flask_session import Session
from test_log import Test_log
import json


__author__ = 'powerup innovation'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

socketio = SocketIO(app, async_mode=None, logger=True, engineio_logger=False)
model = Models()


compressor_teste = Compressor_Teste()

'''
thread = Thread()
thread_stop_event = Event()
'''
#def jigCompressores():

@app.route('/configuracao',methods=["GET","POST"])
def configuracao():
	if request.method == "POST":
		ftp = request.form.get("ftp")
		endereco = request.form.get("endereco")
		ip = request.form.get("ip")
		porta = request.form.get("porta")
		configuracao = model.get_configurations()
		print(configuracao)
		if (configuracao == []):
			print("criando configuracoes")
			model.create_configuration(ftp=ftp,address=endereco,port=porta,ip=ip)
		else:
			print("atualizando configuracoes")	
			model.update_configuration(ftp=ftp,address=endereco,port=porta,ip=ip)	
	configuracoes = model.get_configurations()
	#print(configuracoes[0].ftp+" "+configuracoes.address[0]+" "+configuracoes[0].port+" "+configuracoes[0].ip)
	return render_template('configuracao.html',configuracoes=configuracoes)

@app.route('/cadastro_especificacoes',methods=["GET","POST"])
def cadastro_especificacoes(modelo=None):
	if request.method == "POST":
		codigo = request.form.get("codigo")
		cimax = request.form.get("cimax")
		cimin = request.form.get("cimin")
		vimax = request.form.get("vimax")
		vimin = request.form.get("vimin")
		modo_temperatura = request.form.get("modo_temperatura")
		tipo_alimentacao = request.form.get("tipo_alimentacao")
		voltagem = request.form.get("voltagem")
		print(cimax +"  "+ cimin +" + "+ vimax +" + "+ vimin +" + "+ modo_temperatura)
		model.create_specification(tensao_maximo = 0,
									tensao_minimo = 0,
									corrente_compressor_max = cimax,
									corrente_compressor_min = cimin,
									corrente_ventilador_max = vimax,
									corrente_ventilador_min = vimin,
									pressao_succao_max = 0,
									pressao_succao_min = 0,
									modo_temperatura = modo_temperatura,
									tipo_alimentacao = tipo_alimentacao,
									voltagem = voltagem,
									compressor_code = codigo)
		return especificacoes()
		
	return render_template('cadastro_especificacoes.html')
	
	
@app.route('/especificacoes',methods=["GET","POST"])
def especificacoes():
	if request.method == 'POST':
		#codigo = request.form.get("codigo")
		if request.form.get("editar"):	
			#modelo=model.read_model(codigo)		
			return render_template('cadastro_modelos.html',modelo=modelo)
		elif request.form.get("excluir"):
			print ("excluir")
			#model.delete_model(codigo)       
	else:
		print ("test_2")  
	especificacoes = model.get_specifications()
	print(especificacoes)
	return render_template('especificacoes.html',especificacoes=especificacoes)
	
@app.route('/cadastro_modelos/<modelo>')	
@app.route('/cadastro_modelos',methods=["GET","POST"])
def cadastro_modelos(modelo=None):
	if request.method == "POST":
		codigo = request.form.get("codigo")
		cliente = request.form.get("cliente")
		nome = request.form.get("nome")
		btus = request.form.get("btus")
		modo_temperatura = request.form.get("modo_temperatura")
		tipo_alimentacao = request.form.get("tipo_alimentacao")
		voltagem = request.form.get("voltagem")
		print(codigo+","+nome+","+btus+","+modo_temperatura+","+tipo_alimentacao+","+voltagem)
		if codigo and nome and modo_temperatura and tipo_alimentacao and voltagem:
			modelo=model.read_model(codigo)
			if modelo:
				print("editando")
				model.update_model(nome=nome,cliente=cliente,codigo=codigo,btus=btus,modo_temperatura=modo_temperatura,tipo_alimentacao=tipo_alimentacao,voltagem=voltagem)				
			else:
				print("criando novo")	
				model.create_model(nome=nome,cliente=cliente, codigo=codigo,btus=btus,modo_temperatura=modo_temperatura,tipo_alimentacao=tipo_alimentacao,voltagem=voltagem)
			return modelos()			
	return render_template('cadastro_modelos.html')


@app.route('/modelos',methods=["GET","POST"])
def modelos():
	if request.method == 'POST':
		codigo = request.form.get("codigo")
		if request.form.get("editar"):	
			modelo=model.read_model(codigo)		
			return render_template('cadastro_modelos.html',modelo=modelo)
		elif request.form.get("excluir"):
			print ("excluir")
			model.delete_model(codigo)       
	else:
		print ("test_2")  
	modelos_compressores = model.get_all_models()
	#print(modelos)
	return render_template('modelos.html',modelos_compressores=modelos_compressores)
	
@app.route('/',methods=["GET","POST"])
def index():
	barcode= None
	modelo = None
	testes = []
	if request.method == 'POST':
		print("Executar teste")
		barcode = request.form.get("barcode")				
		if barcode:		
			compressor_teste.ler_codigo_barras(barcode)
			modelo = model.read_model(barcode)
			
	logs = model.read_test_log_all()
	indice =0
	if logs:
		for teste in logs:
			indice = indice + 1
			testes.append(teste.resultado)
	for i in range(15-indice):
		testes.append("Passed")
	return render_template('index.html',modelo=modelo,barcode=barcode,testes=testes)
	
def messageReceived(methods=['GET', 'POST']):
    print('message was received!!!')
	
@socketio.on('teste',namespace='/')
def handle_teste(txt):	
	print ("testando socket"+str(txt))
	emit('teste_log',{'mensagem':"Iniciando","valor_medido":0.0,"resultado":True},callback=messageReceived)
	compressor_teste.andon("TESTANDO")
	configuracao = model.get_configurations()
	print(configuracao)
	for passo in passos_teste:
		print("passo {passo}",passo)
		resultado=compressor_teste.testar(passo)
		print (resultado)
		emit('teste_log',resultado,callback=messageReceived)
		if resultado['resultado']==False:
			compressor_teste.andon("REPROVADO")
			emit('teste_log',{'mensagem':"Finalizando","valor_medido":0.0,"resultado":False},callback=messageReceived)
			teste_log = Test_log(serial=resultado['serial'],
						cliente=resultado['cliente'],
						maquina=configuracao.maquina,
						fvt=configuracao.fvt,
						resultado="Fail",
						sintoma=resultado["sintoma"],
						label_medicao=resultado["label_medicao"],
						valor_medicao=resultado["valor_medicao"] ,
						unidade=resultado["unidade"])
			print("arquivo de log criado")
			model.create_teste_log(	serial = resultado['serial'],
									cliente=resultado['cliente'],
									maquina = configuracao.maquina,
									fvt = configuracao.fvt,
									resultado = "Fail",
									sintoma=resultado["sintoma"],
									medicao_label=resultado["label_medicao"],
									medicao_valor=resultado["valor_medicao"] ,
									unidade=resultado["unidade"])
			print("log salvo no banco")
			return
			
			
		sleep(1)
	else:
		compressor_teste.andon("APROVADO")
		emit('teste_log',{'mensagem':"Finalizando","valor_medido":0.0,"resultado":True},callback=messageReceived)
		teste_log = Test_log(serial = resultado['serial'],
									cliente=resultado['cliente'],
									maquina = configuracao.maquina,
									fvt = configuracao.fvt,
									resultado = "Passed",
									sintoma="aprovado",
									label_medicao=resultado["label_medicao"],
									valor_medicao=resultado["valor_medicao"] ,
									unidade=resultado["unidade"])
		model.create_teste_log(serial = resultado['serial'],
									cliente=resultado['cliente'],
									maquina = configuracao.maquina,
									fvt = configuracao.fvt,
									resultado = "Passed",
									sintoma=resultado["sintoma"],
									medicao_label=resultado["label_medicao"],
									medicao_valor=resultado["valor_medicao"] ,
									unidade=resultado["unidade"])


if __name__ == '__main__':
	socketio.run(app)
		
		
s
