LOG_KEYS={"serial":"S",
		"cliente":"C",
		"maquina":"N",
		"fvt":"P",
		"resultado":"T",
		"sintoma":"F",
		"label_medicao":"M",
		"valor_medicao":"d",
		"unidade":"U"}


import os
from models.models import *

class Test_log():
	def __init__(self,serial,cliente,maquina,fvt):
		self.serial = serial
		self.cliente =cliente
		self.maquina =maquina
		self.fvt=fvt		
		self.criar_arquivo_log()
		self.model = Models()
	
	def criar_arquivo_log(self):
		diretorio = os.path.dirname(os.path.realpath('__file__'))
		self.arquivo = os.path.join(diretorio, 'logs/'+self.serial+".txt")		
		arquivo_log = open(self.arquivo,"w")
		parametros=[]		
		parametros.append(LOG_KEYS["serial"]+self.serial+"\n")
		parametros.append(LOG_KEYS["cliente"]+self.cliente+"\n")
		parametros.append(LOG_KEYS["maquina"]+self.serial+"\n")
		arquivo_log = open(self.arquivo,"w", encoding ="utf-8")
		arquivo_log.writelines(parametros)
		arquivo_log.close()	
	
	def adicionar_log_teste(self,resultado,sintoma,descricao,valor,unidade):		
		parametros=[]
		self.resultado = resultado
		parametros.append(LOG_KEYS["resultado"]+str(resultado)+"\n")
		parametros.append(LOG_KEYS["sintoma"]+str(sintoma)+"\n")
		parametros.append(LOG_KEYS["label_medicao"]+str(descricao)+"\n")
		parametros.append(LOG_KEYS["valor_medicao"]+str(valor)+"\n")
		parametros.append(LOG_KEYS["unidade"]+str(unidade))
		arquivo_log = open(self.arquivo,"a")
		arquivo_log.writelines(parametros)
		arquivo_log.close()
		
	def salvar_em_banco(self):
		self.model.create_teste_log( serial=self.serial,
								cliente=self.cliente,
								maquina=self.maquina,
								fvt=self.fvt,
								resultado=self.resultado
								 )
		
		
	

			
if __name__=="__main__":
	teste_log = Test_log(serial=self.serial,
						cliente="jabil",
						maquina="mquina_01",
						fvt="fvt",
						resultado="ök",
						sintoma="sintoma",
						label_medicao="label_medicao",
						valor_medicao="valor_medicao",
						unidade="unidade")
									
		
