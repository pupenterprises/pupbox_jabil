 document.addEventListener('DOMContentLoaded', function () {

  
  var socket = io();
  var barcode_input = document.getElementById('barcode')
  
  console.log(barcode_input.innerHTML)
  if (barcode_input.innerHTML)
  {
    socket.emit('teste',{data: String(barcode_input.innerHTML)}) 
  }
    
  socket.on( 'teste_log', function( msg ) {
      console.log( msg )
      var teste_log = document.getElementById('teste_log')
      var teste_status = document.getElementById('teste_status')
      console.log( "teste_log")
      if (msg.mensagem != "desativado")
      { 
        if (msg.mensagem=="Iniciando")
        {
          teste_status.innerHTML = "EM TESTE"
          teste_status.style.backgroundColor="yellow"
          teste_status.style.color="black"
        }
        if (msg.mensagem=="Finalizando")
        {
          if (msg.resultado==true)
          {
            teste_status.innerHTML = "APROVADO"
            teste_status.style.backgroundColor="green"
            teste_status.style.color="white"
          }
          else
          {
            teste_status.innerHTML = "REPROVADO"
            teste_status.style.backgroundColor="red"
            teste_status.style.color="white"
            
          }          
        }
        var test_popup = document.getElementById('test_popup') 
        var test_dashboard = document.getElementById('test_dashboard')
        console.log(msg.mensagem)
        
        
        if (msg.inspecao_visual == true)
        {
          test_popup.style.zIndex=1;
          test_dashboard.style.zIndex=0;
          
        }else{
          
          test_popup.style.zIndex=-1;
          test_dashboard.style.zIndex=0;
          
        }
        if (msg.logavel==true)
        {
          var teste_resultado = document.getElementById('teste_resultado')        
          var teste_resultado_tbody = document.getElementById('teste_resultado_tbody')
          var linha = document.createElement("tr");
          
          var tag_descricao = document.createElement("td");
          var tag_especificacao = document.createElement("td");
          var tag_lido = document.createElement("td");
          
          tag_descricao.className = "tabela_descricao"
          tag_especificacao.className = "tabela_especificacao"
          tag_lido.className = "tabela_lido"
          
          var descricao = document.createTextNode(msg.mensagem);
          var especificacao = document.createTextNode(msg.especificacoes);
          var lido = document.createTextNode(msg.valor_medicao);
                  
          tag_descricao.appendChild(descricao);
          tag_especificacao.appendChild(especificacao);
          tag_lido.appendChild(lido);
          
          linha.appendChild(tag_descricao);
          linha.appendChild(tag_especificacao);
          linha.appendChild(tag_lido);
          
          teste_resultado_tbody.appendChild(linha);
          
          linha.scrollIntoView(true); 
        }
        //linha.scrollTop = teste_resultado_tbody.scrollHeight;
      }
  })


  barcode_input.addEventListener('keyup', function(e){
    var key = e.which || e.keyCode;
    var socket2 = io();
    if (key == 13) { 
      console.log( barcode_input.value)   
      console.log("tecla enter foi pressionada")  
      socket2.emit('teste',{data: "teste 2"})     
     
    }
  });


});

