import os
import os.path

from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base
import random

from models.tables import *
#from tables import *

class Models():
	def __init__(self):
		basedir = os.path.abspath(os.path.dirname(__file__))
		engine  = create_engine('sqlite:///'+ os.path.join(basedir,'pupbox.db') +"?check_same_thread=False", echo=False)
		#engine  = create_engine('sqlite:///'+ os.path.join(basedir,'pupbox.db'), echo=False)
		Base.metadata.create_all(engine)
		self.session = sessionmaker(bind = engine)
		
		
		
	def report_sent(self, report):
		session = self.session()
		try:
			report = Report_sent(report)
			session.add(report)	
			session.commit()
			session.close()
			return true
		except:
			print("fail to record report sent")
		session.close()
		return false 
		
	def get_configurations(self):
		session = self.session()
		try:
			query = session.query(Configuration).all()
			for configurations in query:
				session.close()
				return configurations
		except:
			raise
			print("fail to get report sent")
		session.close()
		return False 
		
	def create_configuration(self,ftp,address,port,ip,maquina,fvt):
		session = self.session()
		result=0
		try:
			query = session.query(Configuration).count()
			if query == 0:
				new_model = Configuration(	ftp=ftp,
											address=address,
											port=port,
											ip=ip,
											maquina=maquina,
											fvt=fvt
											)
				session.add(new_model)
				session.commit()
				result=0
			else:
				result=1
			session.close()
			return query
		except:
			print("falha ao criar modelo")
			result=2
		session.close()
		return result 
		
	def update_configuration(self,ftp,address,port,ip,maquina,fvt):
		session = self.session()
		result = 0
		try:			
			print(session.query(Configuration).filter(Configuration.id==1).update({	Configuration.ftp:ftp,
														Configuration.address:address,
														Configuration.port:port,
														Configuration.ip:ip,
														Configuration.maquina:maquina,
														Configuration.fvt:fvt}, synchronize_session = False))
			session.commit()
			session.close()
			return result
		except :
			print("Falha no banco")
			raise
			result = 2			
		session.close()
		return result	
		
	def get_all_models(self):
		session = self.session()
		try:
			query = session.query(Compressor_modelo).all()
			session.close()
			return query
		except:
			print("fail to record report sent")
		session.close()
		return false 
		
	def create_model(self,nome,cliente,codigo, btus,modo_temperatura,tipo_alimentacao,voltagem):
		session = self.session()
		result=0
		try:
			query = session.query(Compressor_modelo).filter(Compressor_modelo.codigo==codigo).count()
			if query == 0:
				new_model =Compressor_modelo( nome=nome,cliente=cliente,codigo=codigo,btus=btus,modo_temperatura=modo_temperatura,tipo_alimentacao=tipo_alimentacao,voltagem=voltagem)
				session.add(new_model)
				session.commit()
				result=0
			else:
				result=1
			session.close()
			return query
		except:
			print("falha ao criar modelo")
			result=2
		session.close()
		return result 
		
	def read_model(self,codigo):
		session = self.session()
		result = 0
		try:
			query = session.query(Compressor_modelo).filter(Compressor_modelo.codigo==codigo).all()
			for product in query:
				session.close()	
				return product
			else:
				session.close()
				return None
		except :
			print("Falha ao ler produto")
		session.close()
		return None
		
	def update_model(self,nome,cliente, codigo, btus,modo_temperatura,tipo_alimentacao,voltagem):
		session = self.session()
		result = 0
		try:
						
			print(session.query(Compressor_modelo).filter(Compressor_modelo.codigo==codigo).update({Compressor_modelo.nome:nome,
																									Compressor_modelo.cliente:cliente,
																									Compressor_modelo.codigo:codigo,
																									Compressor_modelo.btus:btus,
																									Compressor_modelo.modo_temperatura:modo_temperatura, 
																									Compressor_modelo.tipo_alimentacao:tipo_alimentacao,																									 
																									Compressor_modelo.voltagem:voltagem}, synchronize_session = False))
			session.commit()
			session.close()
			return result
		except :
			print("Falha no banco")
			#raise
			result = 2			
		session.close()
		return result	
	
	def delete_model(self,codigo):
		session = self.session()
		result = 0
		try:			
			query = session.query(Compressor_modelo).filter(Compressor_modelo.codigo==codigo).all()
			for product in query:
				session.delete(product)
				session.commit()
				session.close()
			return result
		except :
			print("Falha no banco")
			#raise
			result = 2			
		session.close()
		return result
		
	def get_specifications(self):
		session = self.session()
		try:
			query = session.query(Teste_Especificacao).all()
			session.close()
			return query
		except:
			print("fail to record report sent")
		session.close()
		return false 
	
	def create_specification(	self,
								perifericos_testar,
								perifericos_voltagem_v1_min,
								perifericos_voltagem_v1_max,
								perifericos_voltagem_v2_min,
								perifericos_voltagem_v2_max,
								perifericos_voltagem_v3_min,
								perifericos_voltagem_v3_max,
								partida_compressor_testar,
								partida_compressor_tempo,
								corrente_compressor_testar,
								corrente_compressor_repetir,						
								corrente_compressor_l1_min,
								corrente_compressor_l1_max,
								corrente_compressor_l2_min,
								corrente_compressor_l2_max,
								corrente_compressor_l3_min,
								corrente_compressor_l3_max,	
								inpecao_visual_testar,
								inpecao_visual_tempo,					
								compressor_code ):
		session = self.session()
		result=0
		try:
			new_spec =Teste_Especificacao(	perifericos_testar = perifericos_testar,
											perifericos_voltagem_v1_min = perifericos_voltagem_v1_min,
											perifericos_voltagem_v1_max = perifericos_voltagem_v1_max,
											perifericos_voltagem_v2_min = perifericos_voltagem_v2_min,
											perifericos_voltagem_v2_max = perifericos_voltagem_v2_max,
											perifericos_voltagem_v3_min = perifericos_voltagem_v3_min,
											perifericos_voltagem_v3_max = perifericos_voltagem_v3_max,
											partida_compressor_testar = partida_compressor_testar,
											partida_compressor_tempo = partida_compressor_tempo,
											corrente_compressor_testar = corrente_compressor_testar,
											corrente_compressor_repetir = corrente_compressor_repetir,						
											corrente_compressor_l1_min = corrente_compressor_l1_min,
											corrente_compressor_l1_max = corrente_compressor_l1_max,
											corrente_compressor_l2_min = corrente_compressor_l2_min,
											corrente_compressor_l2_max = corrente_compressor_l2_max,
											corrente_compressor_l3_min = corrente_compressor_l3_min,
											corrente_compressor_l3_max = corrente_compressor_l3_max,	
											inpecao_visual_testar = inpecao_visual_testar,
											inpecao_visual_tempo = inpecao_visual_tempo,					
											compressor_code = compressor_code	)
			session.add(new_spec)
			session.commit()
			session.close()
			print("Nova especificacao salva com sucesso")
			result = 1			
			return result
		except:
			raise
			print("falha ao criar especificacao")
			result=2
		session.close()
		return result 
		
	def read_specification (self,compressor_code):
		session = self.session()
		result = 0
		try:
			query = session.query(Teste_Especificacao).filter(Teste_Especificacao.compressor_code==compressor_code).all()
			for product in query:
				session.close()	
				return product
			else:
				session.close()
				return None
		except :
			raise
			print("Falha ao ler produto")
		session.close()
		return None
		
	def update_specification(	self,
								perifericos_testar,
								perifericos_voltagem_v1_min,
								perifericos_voltagem_v1_max,
								perifericos_voltagem_v2_min,
								perifericos_voltagem_v2_max,
								perifericos_voltagem_v3_min,
								perifericos_voltagem_v3_max,
								partida_compressor_testar,
								partida_compressor_tempo,
								corrente_compressor_testar,
								corrente_compressor_repetir,						
								corrente_compressor_l1_min,
								corrente_compressor_l1_max,
								corrente_compressor_l2_min,
								corrente_compressor_l2_max,
								corrente_compressor_l3_min,
								corrente_compressor_l3_max,	
								inpecao_visual_testar,
								inpecao_visual_tempo,					
								compressor_code ):
		session = self.session()
		result = 0
		try:
						
			print(session.query(Teste_Especificacao).filter(Teste_Especificacao.compressor_code==compressor_code).update({Teste_Especificacao.perifericos_testar : perifericos_testar,
																															Teste_Especificacao.perifericos_voltagem_v1_min : perifericos_voltagem_v1_min,
																															Teste_Especificacao.perifericos_voltagem_v1_max : perifericos_voltagem_v1_max,
																															Teste_Especificacao.perifericos_voltagem_v2_min : perifericos_voltagem_v2_min,
																															Teste_Especificacao.perifericos_voltagem_v2_max : perifericos_voltagem_v2_max,
																															Teste_Especificacao.perifericos_voltagem_v3_min : perifericos_voltagem_v3_min,
																															Teste_Especificacao.perifericos_voltagem_v3_max : perifericos_voltagem_v3_max,	
																															Teste_Especificacao.partida_compressor_testar : partida_compressor_testar,
																															Teste_Especificacao.partida_compressor_tempo : partida_compressor_tempo,
																															Teste_Especificacao.corrente_compressor_testar : corrente_compressor_testar,
																															Teste_Especificacao.corrente_compressor_repetir : corrente_compressor_repetir,						
																															Teste_Especificacao.corrente_compressor_l1_min : corrente_compressor_l1_min,
																															Teste_Especificacao.corrente_compressor_l1_max : corrente_compressor_l1_max,
																															Teste_Especificacao.corrente_compressor_l2_min : corrente_compressor_l2_min,
																															Teste_Especificacao.corrente_compressor_l2_max : corrente_compressor_l2_max,
																															Teste_Especificacao.corrente_compressor_l3_min : corrente_compressor_l3_min,
																															Teste_Especificacao.corrente_compressor_l3_max : corrente_compressor_l3_max,	
																															Teste_Especificacao.inpecao_visual_testar : inpecao_visual_testar,
																															Teste_Especificacao.inpecao_visual_tempo : inpecao_visual_tempo,}, 
																															synchronize_session = False))
			session.commit()
			session.close()
			return result
		except :
			print("Falha no banco")
			raise
			result = 2			
		session.close()
		return result
		
	def delete_spec(self,codigo):
		session = self.session()
		result = 0
		try:			
			query = session.query(Teste_Especificacao).filter(Teste_Especificacao.compressor_code==codigo).all()
			for product in query:
				session.delete(product)
				session.commit()
				session.close()
			return result
		except :
			print("Falha no banco")
			#raise
			result = 2			
		session.close()
		return result

	def create_teste_log(self, serial,cliente,maquina,fvt,resultado,sintoma=None,medicao_label=None,medicao_valor=None,unidade=None):
		session = self.session()
		result=0
		print("Salvando novo log")
		try:
			new_log =Teste_log( 	serial=serial, 
									cliente=cliente, 
									maquina=maquina, 
									fvt=fvt,
									resultado=resultado, 
									sintoma=sintoma,
									medicao_label=medicao_label, 
									medicao_valor=medicao_valor, 
									unidade=unidade)
			session.add(new_log)
			session.commit()
			session.close()
			print("Log salvo com sucesso")
			return True
		except:
			raise
			print("falha ao criar log teste")
			result=2
		session.close()
		return result 
		
	def read_test_log(self,serial):
		session = self.session()
		result = 0
		try:
			query = session.query(Teste_log).filter(Teste_log.serial==serial).all()
			session.close()	
			for product in query:				
				return product
			else:
				return None
		except :
			print("Falha ao ler produto")
		session.close()
		return None
		
	def read_test_log_all(self):
		session = self.session()
		result = 0
		try:
			query = session.query(Teste_log).order_by(Teste_log.id.desc()).limit(15)
			session.close()	
			for product in query:				
				return query
			else:
				return None
		except :
			print("Falha ao ler produto")
		session.close()
		return None
	
	
		
		
if __name__ == "__main__":
	tmodel = Models()
	#tmodel.create_configuration(ftp="",address="",port="",ip="", jig_id="",maquina="Maquina 01",fvt="Functional Verification Teste")
	#configuracao = tmodel.get_configurations()
	'''
	tmodel.create_specification(	perifericos_testar=True,
								perifericos_voltagem_v1_min=192,
								perifericos_voltagem_v1_max=242,
								perifericos_voltagem_v2_min=0,
								perifericos_voltagem_v2_max=0,
								perifericos_voltagem_v3_min=0,
								perifericos_voltagem_v3_max=0,
								partida_compressor_testar=True,
								partida_compressor_tempo=5,
								corrente_compressor_testar=True,
								corrente_compressor_repetir=30,						
								corrente_compressor_l1_min=7.5,
								corrente_compressor_l1_max=14,
								corrente_compressor_l2_min=0,
								corrente_compressor_l2_max=0,
								corrente_compressor_l3_min=0,
								corrente_compressor_l3_max=0,	
								inpecao_visual_testar=True,
								inpecao_visual_tempo=5,					
								compressor_code="RAP36F3L" )
	'''
	specs=tmodel.read_specification("RAP36F3L" )
	print(specs.perifericos_voltagem_v1_min)
	print(specs.perifericos_voltagem_v1_max)
	for i in range(10):
		tmodel.delete_spec("RAP36F3L" )
	
	#for configuracao in configuracoes:
	#print(configuracao)	
	#print(configuracao.maquina)	
	'''
	tmodel.create_model(nome="teste",
						codigo="C045671",
						btus="10K",
						modo_temperatura="QUENTE",
						tipo_alimentacao="BIFASICO",
						voltagem="220")
	tmodel.create_specification(tensao_maximo = 0,
									tensao_minimo = 0,
									corrente_compressor_max = 2,
									corrente_compressor_min = 1,
									corrente_ventilador_max = 2,
									corrente_ventilador_min = 1,
									pressao_succao_max = 0,
									pressao_succao_min = 0,
									modo_temperatura = "QUENTE",
									tipo_alimentacao = "bifasica",
									voltagem = "220",
									compressor_code = "C045671")
	'''
