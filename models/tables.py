from sqlalchemy import Column, ForeignKey, Integer, Float, String,Boolean, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
import random
import os
import os.path
import datetime

Base = declarative_base()


class Compressor_modelo(Base):
	__tablename__ = "compressor_modelo"
	id = Column(Integer, primary_key = True)
	nome = Column(String)
	cliente = Column(String)
	codigo = Column(String)
	btus = Column(String)
	modo_temperatura= Column(String)
	tipo_alimentacao= Column(String)
	voltagem= Column(String)
	teste_especificacao = relationship('Teste_Especificacao')
	
	def __init__(self, nome,cliente,codigo,btus,modo_temperatura,tipo_alimentacao,voltagem):
		self.nome = nome
		self.cliente = cliente
		self.codigo = codigo
		self.btus = btus
		self.modo_temperatura = modo_temperatura
		self.tipo_alimentacao = tipo_alimentacao
		self.voltagem = voltagem

class Teste_Especificacao(Base):
	__tablename__ = "teste_especificacao"
	id = Column(Integer, primary_key = True)
	perifericos_testar = Column(Boolean)
	perifericos_voltagem_v1_min = Column(Float)
	perifericos_voltagem_v1_max = Column(Float)
	perifericos_voltagem_v2_min = Column(Float)
	perifericos_voltagem_v2_max = Column(Float)
	perifericos_voltagem_v3_min = Column(Float)
	perifericos_voltagem_v3_max = Column(Float)
	partida_compressor_testar = Column(Boolean)
	partida_compressor_tempo = Column (Integer)	
	corrente_compressor_testar = Column(Boolean)
	corrente_compressor_repetir = Column (Integer)	
	corrente_compressor_l1_min = Column(Float)
	corrente_compressor_l1_max = Column(Float)	
	corrente_compressor_l2_min = Column(Float)
	corrente_compressor_l2_max = Column(Float)	
	corrente_compressor_l3_min = Column(Float)
	corrente_compressor_l3_max = Column(Float)
	inpecao_visual_testar = Column (Boolean)
	inpecao_visual_tempo = Column (Integer)
	compressor_code = Column(String,ForeignKey('compressor_modelo.codigo'),nullable=True )
	
	
	def __init__(self,  perifericos_testar,
						perifericos_voltagem_v1_min,
						perifericos_voltagem_v1_max,
						perifericos_voltagem_v2_min,
						perifericos_voltagem_v2_max,
						perifericos_voltagem_v3_min,
						perifericos_voltagem_v3_max,
						partida_compressor_testar,
						partida_compressor_tempo,
						corrente_compressor_testar,
						corrente_compressor_repetir,						
						corrente_compressor_l1_min,
						corrente_compressor_l1_max,
						corrente_compressor_l2_min,
						corrente_compressor_l2_max,
						corrente_compressor_l3_min,
						corrente_compressor_l3_max,	
						inpecao_visual_testar,
						inpecao_visual_tempo,					
						compressor_code ):
		self.perifericos_testar = perifericos_testar
		self.perifericos_voltagem_v1_min = perifericos_voltagem_v1_min
		self.perifericos_voltagem_v1_max = perifericos_voltagem_v1_max
		self.perifericos_voltagem_v2_min = perifericos_voltagem_v2_min
		self.perifericos_voltagem_v2_max = perifericos_voltagem_v2_max
		self.perifericos_voltagem_v3_min = perifericos_voltagem_v3_min
		self.perifericos_voltagem_v3_max = perifericos_voltagem_v3_max
		self.partida_compressor_testar = partida_compressor_testar
		self.partida_compressor_tempo = partida_compressor_tempo					
		self.corrente_compressor_testar = corrente_compressor_testar
		self.corrente_compressor_repetir = corrente_compressor_repetir
		self.corrente_compressor_l1_min = corrente_compressor_l1_min
		self.corrente_compressor_l1_max = corrente_compressor_l1_max
		self.corrente_compressor_l2_min = corrente_compressor_l2_min
		self.corrente_compressor_l2_max = corrente_compressor_l2_max
		self.corrente_compressor_l3_min = corrente_compressor_l3_min
		self.corrente_compressor_l3_max = corrente_compressor_l3_max
		self.inpecao_visual_testar = inpecao_visual_testar
		self.inpecao_visual_tempo = inpecao_visual_tempo
		self.compressor_code = compressor_code
		
class Teste_resultado(Base):
	__tablename__ = "teste_resultados"
	id = Column(Integer, primary_key = True)
	corrente_compressor = Column(String)
	corrente_ventilador = Column(String)
	pressao= Column(String)
	tipo_alimentacao= Column(String)
	voltagem= Column(String)
	
	resultado = Column (String)
	
	def __init__(self, nome,btus,modo_temperatura,tipo_alimentacao,voltagem):
		self.nome = nome
		self.btus = btus
		self.nome = modo_temperatura
		self.btus = tipo_alimentacao
		self.btus = tipo_alimentacao
		self.voltagem = voltagem

class Teste_log(Base):
	__tablename__ = "teste_log"
	id = Column(Integer, primary_key = True)
	serial = Column(String)
	cliente = Column(String)
	maquina= Column(String)
	fvt= Column(String)
	resultado= Column(String)
	sintoma= Column(String)
	medicao_label= Column(String)
	medicao_valor= Column(String)	
	unidade = Column (String)
	
	def __init__(self, serial=None,cliente=None,maquina=None,fvt=None,resultado=None,sintoma=None,medicao_label=None,medicao_valor=None,unidade=None):
		self.serial = serial
		self.cliente = cliente
		self.maquina = maquina
		self.fvt = fvt
		self.resultado = resultado
		self.sintoma = sintoma
		self.medicao_label = medicao_label
		self.medicao_valor = medicao_valor
		self.unidade = unidade
		
		
class Report_sent(Base):
	__tablename__ = "report_sent"
	id = Column(Integer, primary_key = True)
	report_timestamp = Column(String)
	report_sent= Column(String)
	
	def __init__(self, report_timestamp,report_sent):
		self.report_timestamp = report_timestamp
		self.report_sent = report_sent
'''
		teste_log = Test_log(serial="12345",
						cliente="jabil",
						maquina="mquina_01",
						fvt="fvt",
						resultado="Passed",
						sintoma="sintoma",
						label_medicao="label_medicao",
						valor_medicao="valor_medicao",
						unidade="unidade")
'''	

class Configuration(Base):
	__tablename__ = "configuration"
	id = Column(Integer, primary_key = True)
	maquina=Column(String)
	fvt = Column(String)	
	ftp = Column(String)
	address = Column(String)
	port = Column(String)
	ip = Column(String)
	maquina = Column(String)
	fvt = Column(String)
	
	def __init__(self, ftp=None,address=None,port=None, ip=None,maquina = None, fvt = None):
		self.ftp = ftp
		self.address = address
		self.port = port
		self.ip = ip
		self.maquina = maquina
		self.fvt = fvt

