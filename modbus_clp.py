import minimalmodbus
from time import sleep

botoeiras = {
			  "INICIAR":256,
			  "APROVAR":   257,
			  "REPROVAR":258
			}
			
comandos = { 
			 "380V_TRIFASICO": 1280,
			 "220V_TRIFASICO": 1281,
			 "220V_MONOFASICO": 1282,
			 "VALVULA_REVERSORA": 1283,
			 "SINALIZACAO_380_TRIFASICA": 1285,	
			 "SINALIZACAO_220_TRIFASICA": 1286,	
			 "SINALIZACAO_220_MONOFASICA": 1287,
			 "ANDON_AMARELO": 1288,	
			 "ANDON_VERDE": 1289,	
			 "ANDON_VERMELHO": 1290,			 					
			}

instrument = minimalmodbus.Instrument('/dev/ttyUSB0',2)
instrument.serial.baudrate = 19200
instrument.serial.timeout = 1

while True:
	for i in range(8):
		print(f"x{i} {instrument.read_bit(1024+i,2)}")
	sleep(1)


